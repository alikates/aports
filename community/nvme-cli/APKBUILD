# Contributor: Florian Heigl <florian.heigl@gmail.com>
# Maintainer: Florian Heigl <florian.heigl@gmail.com>
pkgname=nvme-cli
pkgver=2.1.1
pkgrel=0
pkgdesc="NVM-Express user space tooling for Linux"
arch="all"
url="https://github.com/linux-nvme/nvme-cli"
license="GPL-2.0-only"
makedepends="
	bash
	libnvme-dev
	linux-headers
	meson
	util-linux-dev
	uuidgen
	zlib-dev
	"
checkdepends="
	py3-nose2
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/linux-nvme/nvme-cli/archive/v$pkgver.tar.gz"
options="!check" # needs pci nvme stuff

build() {
	abuild-meson \
		-Ddocs=man \
		-Dudevrulesdir=/usr/lib/udev/rules.d/ \
		. output
	meson compile -C output
}

check() {
	meson test -v --no-rebuild -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
	rm -rf "$pkgdir"/usr/lib/systemd
}

sha512sums="
61a8e7c0ae8da082ac0cda2fdf61ffe8d83ed5d4ee31640bde89fde929ba3fe6d788950bd232630af2c7fc5f3563f05f47f350e005c8556691e5aede73c88d74  nvme-cli-2.1.1.tar.gz
"
